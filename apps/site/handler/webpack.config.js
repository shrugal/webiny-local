const path = require("path");
const webpack = require("webpack");
const WebpackBar = require("webpackbar");
const aliases = require("@webiny/project-utils/aliases/webpack");

module.exports = {
  entry: path.resolve("handler", "ssr.js"),
  output: {
    filename: "ssr.js",
    path: path.resolve("build-handler"),
    libraryTarget: "commonjs",
    publicPath: process.env.PUBLIC_URL
  },
  resolve: {
    alias: {
      ...aliases,
      webfontloader: "null-loader",
      "react-spinner-material": "null-loader"
    },
    extensions: [".js", ".json", ".jsx", ".mjs"]
  },
  target: "node",
  node: {
    __dirname: false
  },
  optimization: {
    // We no not want to minimize our code.
    minimize: false
  },
  plugins: [
    new WebpackBar({ name: "Site SSR handler" }),
    new webpack.EnvironmentPlugin([
      "REACT_APP_GRAPHQL_API_URL",
      "REACT_APP_ENV",
      "PUBLIC_URL"
    ])
  ],
  module: {
    rules: [
      {
        oneOf: [
          {
            test: [/\.mjs$/, /\.js$/, /\.jsx$/],
            exclude: /node_modules/,
            loader: "babel-loader",
            options: {
              presets: [
                [
                  "@babel/preset-env",
                  {
                    targets: { node: "10.14" },
                    modules: false,
                    exclude: ["transform-typeof-symbol"]
                  }
                ],
                ["@babel/preset-react", { useBuiltIns: true }]
              ],
              plugins: ["babel-plugin-lodash"]
            }
          },
          {
            test: [/\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.png$/],
            loader: "url-loader",
            options: {
              limit: 10000,
              name: "static/media/[name].[hash:8].[ext]"
            }
          },
          {
            exclude: [/\.mjs$/, /\.js$/, /\.html$/, /\.json$/],
            loader: "file-loader",
            options: {
              name: "static/media/[name].[hash:8].[ext]",
              publicPath: process.env.PUBLIC_URL,
              // Do not emit file since this is a server-side render
              emitFile: false
            }
          }
        ]
      }
    ]
  }
};
