const express = require("express")
const cacheableResponse = require("cacheable-response")
const chalk = require("chalk")

const ssrCache = cacheableResponse({
  get: async ({ req }) => {
    try {
      delete require.cache[require.resolve("./ssr")]
      const { ssr } = require("./ssr")
      const { html } = await ssr(req.path)

      return {
        statusCode: 200,
        data: html,
        ttl: 2 * 60 * 60 * 1000,
      }
    } catch (e) {
      console.error(e)
      return { statusCode: 500, data: null, ttl: 0 }
    }
  },
  send: ({ res, statusCode, data }) => {
    res
      .status(statusCode)
      .header({ "Content-Type": "text/html" })
      .send(data)
  },
})

const app = express()
app.use(express.static("build"))
app.use((req, res) => ssrCache({ req, res }))

const port = process.env.PORT || 3002
app.listen(port, () => {
  console.log(
    `${chalk.cyan(`🚀 Site running on port ${port}...`)} ${chalk.grey(
      "(Hit Ctrl+C to abort)"
    )}`
  )
})
