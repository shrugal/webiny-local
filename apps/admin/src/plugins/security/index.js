import React from "react"
import localStorage from "store"
import Authentication from "./Authentication"

export default {
    name: "security-authentication-provider-cognito",
    type: "security-authentication-provider",
    securityProviderHook({ onIdToken }) {
        const renderAuthentication = ({ viewProps = {} } = {}) => {
            return <Authentication onIdToken={onIdToken} {...viewProps} />;
        };

        const logout = () => {}
        const getIdToken = async () => localStorage.get("webiny-token")

        return { getIdToken, renderAuthentication, logout };
    }
}