import React, { useState } from "react";
import { getPlugin } from "@webiny/plugins";
import SignInState from "./SignInState";
import invariant from "invariant";

const Authentication = ({ onIdToken, ...viewProps }) => {
  const [state, setState] = useState({ authState: "signIn" });

  const { view: SignIn } = getPlugin("cognito-view-sign-in");
  invariant(SignIn, `Missing "cognito-view-sign-in" plugin!`);

  const changeState = (authState, authData, message) => {
    if (authState === "signedIn" && authData) {
      onIdToken(authData);
    }

    setState({ authState, authData, message });
  };

  return (
    <SignInState {...state} changeState={changeState}>
      <SignIn {...viewProps} />
    </SignInState>
  );
};

export default Authentication;
