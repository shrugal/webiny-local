import React, { useState } from "react"

const authStates = ["signIn", "signedOut", "signedUp"]

const SignIn = ({ changeState, children, ...authProps }) => {
  const error = useState(null)[0]
  const [loading, setLoading] = useState(false)

  if (!authStates.includes(authProps.authState)) {
    return null
  }

  const signIn = input => {
    setLoading(true)
    changeState("signedIn", input.username + ":" + input.password)
  }

  return React.cloneElement(children, {
    authProps,
    signIn,
    changeState,
    error,
    loading,
  })
}

export default SignIn
