import { fileUploadPlugin, imagePlugin } from "@webiny/app/plugins";
import adminPlugins from "@webiny/app-admin/plugins";
import i18nPlugins from "@webiny/app-i18n/admin/plugins";
import securityPlugins from "@webiny/app-security/admin/plugins";
import pageBuilderPlugins from "@webiny/app-page-builder/admin/plugins";
import pageBuilderThemePlugins from "@webiny/app-page-builder-theme";
import formBuilderPlugins from "@webiny/app-form-builder/admin/plugins";
import formBuilderPageBuilderPlugins from "@webiny/app-form-builder/page-builder/admin/plugins";
import formBuilderTheme from "@webiny/app-form-builder-theme";
import cookiePolicyPlugins from "@webiny/app-cookie-policy/admin";
import googleTagManagerPlugins from "@webiny/app-google-tag-manager/admin";
import typeformPlugins from "@webiny/app-typeform/admin";
import mailchimpPlugins from "@webiny/app-mailchimp/admin";
import security from "./security";
import cognitoTheme from "@webiny/app-plugin-security-cognito-theme/admin";

import pbInstallPlugin from "./pbInstall";

const plugins = [
  fileUploadPlugin({}),
  imagePlugin,
  adminPlugins,
  i18nPlugins,
  securityPlugins,
  pageBuilderPlugins,
  pageBuilderThemePlugins(),
  formBuilderPlugins,
  formBuilderPageBuilderPlugins,
  formBuilderTheme(),
  cookiePolicyPlugins,
  googleTagManagerPlugins,
  typeformPlugins,
  mailchimpPlugins,
  security,
  cognitoTheme(),
  pbInstallPlugin
];

export default plugins;
