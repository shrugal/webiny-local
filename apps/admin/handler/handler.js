const express = require("express")
const chalk = require("chalk")

const app = express()

const router = express
  .Router()
  .use(express.static("build"))
  .get("*", (req, res) => {
    res.sendFile("index.html", { root: "build" })
  })
app.use("/admin", router).use("/", router)

const port = process.env.PORT || 3001
app.listen(port, () => {
  console.log(
    `${chalk.cyan(`🚀 Admin running on port ${port}...`)} ${chalk.grey(
      "(Hit Ctrl+C to abort)"
    )}`
  )
})
