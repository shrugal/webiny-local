module.exports = () => ({
  name: "graphql-context-pb-install",
  type: "graphql-context",
  async apply(context) {
    // Skip page-builder example data installation
    const settings = await context.models.PbSettings.load();
    if (settings && !settings.data.installation.completed) {
      for (let i = 1; i <= 4; i++) {
        settings.data.installation.getStep(i).markAsCompleted();
      }
    }
  }
});
