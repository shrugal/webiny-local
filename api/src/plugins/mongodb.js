const { MongoClient } = require("mongodb")
const {
  MongoDbDriver: MongoDbDriverBase,
  id,
} = require("@commodo/fields-storage-mongodb")

class MongoDbDriver extends MongoDbDriverBase {
  getClient() {
    return {
      runOperation: ({ collection, operation }) => {
        const [operationName, ...args] = operation
        const collectionInstance = this.getDatabase().collection(collection)

        if (operationName === "find") {
          const [findFilters, findRestArgs] = args
          const results = collectionInstance.find(findFilters)
          if (findRestArgs.limit) {
            results.limit(findRestArgs.limit)
          }
          if (findRestArgs.offset) {
            results.skip(findRestArgs.offset)
          }
          if (findRestArgs.sort) {
            results.sort(findRestArgs.sort)
          }

          return results.toArray()
        }

        if (operationName === "aggregate") {
          return collectionInstance.aggregate(...args).toArray()
        }

        return collectionInstance[operationName](...args)
      },
    }
  }
}

let database = null
let client = null

async function getDatabase({ server, name }) {
  const client = await MongoClient.connect(server, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })

  return { database: client.db(name), client }
}

module.exports = options => ({
  name: "graphql-context-commodo",
  type: "graphql-context",
  async preApply(context) {
    if (!context.commodo) {
      context.commodo = {}
    }

    if (!context.commodo.fields) {
      context.commodo.fields = {}
    }

    context.commodo.fields.id = id

    if (process.env.NODE_ENV === "test") {
      // During tests, references to client and database are handled differently.
      // We need to keep them in local scope to be able to cleanup.
      const refs = await getDatabase(options.database)
      context.commodo.driver = new MongoDbDriver({ database: refs.database })

      if (options.test) {
        options.test.afterAll(async () => {
          await refs.client.close()
        })
      }

      return
    }

    if (!client) {
      const refs = await getDatabase(options.database)
      client = refs.client
      database = refs.database
    }
    context.commodo.driver = new MongoDbDriver({ database })
  },
})
