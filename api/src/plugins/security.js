const gql = require("graphql-tag");
const util = require("util")
const bcrypt = require("bcryptjs");

const verify = util.promisify(bcrypt.compare);

module.exports = () => {
    return [
        {
            type: "graphql-schema",
            name: "graphql-schema-cognito",
            schema: {
                typeDefs: gql`
                    extend input SecurityInstallInput {
                        password: String
                    }

                    extend input SecurityUserInput {
                        password: String
                    }

                    # This input type is used by the user who is updating his own account
                    extend input SecurityCurrentUserInput {
                        password: String
                    }
                `
            }
        },
        {
            name: "security-authentication-provider-cognito",
            type: "security-authentication-provider",
            async userFromToken({ idToken, SecurityUser }) {
                const [email, password] = idToken.split(":", 2)

                const user = await SecurityUser.findOne({ query: { email } });

                if(!user) {
                    return null
                } else if (!verify(password, user.password)) {
                    throw Object.assign(error, {
                        code: "SECURITY_COGNITO_INVALID_TOKEN"
                    });
                }

                return user;
            },
            async createUser() {},
            async updateUser({ data, user }) {
                // Fixes the bug that the updateUser resolver doesn't properly
                // pupulate the save data
                return user.populate(data).save()
            },
            async deleteUser() {},
            async countUsers() {
                // This just needs to be > 0
                return 1
            }
        }
    ];
};
