const { Component } = require("@serverless/core");
const normalizeInputs = require("@webiny/serverless-files/utils/normalizeInputs");

/**
 * This component deploys:
 * - S3 bucket for file storage
 * - API GW with "/files/{key}" route
 * - Three functions:
 * - manage files - when a file is deleted, this makes sure all other related files are deleted too
 * - download files - handles file download and calls image transformer if needed
 * - image transformer - performs various image transformations
 */

class FilesComponent extends Component {
    async default(rawInputs = {}) {
        const inputs = normalizeInputs(rawInputs);

        const {
            region,
            bucket,
            functions: {
                apolloService: apolloServiceInputs,
            }
        } = inputs;

        if (!apolloServiceInputs.plugins) {
            apolloServiceInputs.plugins = [];
        }

        // Deploy graphql API
        const apolloService = await this.load(__dirname + "/apolloService");
        const apolloServiceOutput = await apolloService({
            ...apolloServiceInputs,
            region,
            env: {
                ...apolloServiceInputs.env,
                S3_BUCKET: bucket,
                DEBUG: apolloServiceInputs.debug || "true",
                UPLOAD_MIN_FILE_SIZE: String(apolloServiceInputs.uploadMinFileSize),
                UPLOAD_MAX_FILE_SIZE: String(apolloServiceInputs.uploadMaxFileSize)
            }
        });

        const output = {
            api: apolloServiceOutput.api
        };

        this.state.output = output;
        await this.save();

        return output;
    }

    async remove() {
        const apolloService = await this.load(__dirname + "/apolloService");
        await apolloService.remove();

        this.state = {};
        await this.save();
    }
}

module.exports = FilesComponent;
