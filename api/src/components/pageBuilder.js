const { Component } = require("@serverless/core");
const get = require("lodash.get");

class ServerlessPageBuilder extends Component {
    async default(inputs = {}) {
        const { plugins = [], env, files, ...rest } = inputs;

        const apolloService = await this.load(__dirname + "/apolloService");
        const output = await apolloService({
            plugins,
            env: {
                ...env,
                FILES_API_URL: get(files, "api.graphqlUrl"),
            },
            ...rest
        });

        this.state.output = output;
        await this.save();

        return output;
    }

    async remove(inputs = {}) {
        const apolloService = await this.load(__dirname + "/apolloService");
        await apolloService.remove(inputs);

        this.state = {};
        await this.save();
    }
}

module.exports = ServerlessPageBuilder;
