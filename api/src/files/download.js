const fs = require("fs-extra")
const sharp = require("sharp")
const mime = require("mime-types")
const path = require("path")

const UPLOADS_FOLDER = process.env.UPLOADS_FOLDER || "../.files"

module.exports.handler = async ({ pathParameters, queryStringParameters }) => {
  const { key } = pathParameters
  const type = mime.lookup(key)

  try {
    const folder = path.resolve(UPLOADS_FOLDER)

    let buffer = await fs.readFile(path.join(folder, "/", key))
    if (queryStringParameters) {
      // If an image was requested, we can apply additional image processing.
      const width = parseInt(queryStringParameters.width)
      if (width) {
        const supportedImageTypes = ["image/jpg", "image/jpeg", "image/png"]
        if (supportedImageTypes.includes(type)) {
          buffer = await sharp(buffer)
            .resize({ width })
            .toBuffer()
        }
      }
    }

    return {
      statusCode: 200,
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Content-Type": type,
      },
      body: buffer,
    }
  } catch (e) {
    return {
      statusCode: 404,
      headers: {
        "Access-Control-Allow-Origin": "*",
      },
    }
  }
}
